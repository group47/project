from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
    # Examples:
    # url(r'^$', 'cs252.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^user/', include('first.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', include('first.urls')),
    #url(r'.*', include('second.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) 
