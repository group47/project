from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^.*', views.any, name='any_url'),
]
