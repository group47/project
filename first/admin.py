from django.contrib import admin
# from django.contrib.auth.models import User
from . import models
# Register your models here.
# admin.site.register(User)

class ExamInline(admin.TabularInline):
    model = models.Exam
    extra = 0

class ProjectInline(admin.TabularInline):
    model = models.ProjectInternships
    extra = 0

class ScholasticAchievementsInline(admin.TabularInline):
    model = models.ScholasticAchievements
    extra = 0

class TechnicalSkillInline(admin.TabularInline):
    model = models.TechnicalSkill
    extra = 0
 
class ResumeAdmin(admin.ModelAdmin):
    inlines = [
        ExamInline,
        ProjectInline,
        ScholasticAchievementsInline,
        TechnicalSkillInline,
    ]

admin.site.register(models.Resume, ResumeAdmin)
