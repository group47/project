from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

class Exam(models.Model):
    resume = models.ForeignKey('Resume', on_delete=models.CASCADE)
    # academic_detail = models.ForeignKey('academic_details', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, verbose_name='Exam Name')
    year = models.CharField(max_length=20)
    board = models.CharField(max_length=100, blank=True)
    institute = models.CharField(max_length=200)
    cgpa_percentage = models.FloatField(verbose_name='CGPA/Percentage')
    proof = models.FileField(upload_to='exams', null=True)
    verified_choices = (
        (1, 'Not yet verified'),
        (2, 'Verified'),
        (3, 'Wrong Proof'),
    )
    verified = models.IntegerField(choices=verified_choices, default=False)
    spocomment = models.TextField(blank=True)

    def __str__(self):
        return self.name

class AcademicDetails(models.Model):

    def __str__(self):
        return "academic_details " + self.pk

class ScholasticAchievements(models.Model):
    resume = models.ForeignKey('Resume', on_delete=models.CASCADE)
    string = models.TextField(verbose_name='Achievement')
    proof = models.FileField(upload_to='scholastic', null=True)
    verified_choices = (
        (1, 'Not yet verified'),
        (2, 'Verified'),
        (3, 'Wrong Proof'),
    )
    verified = models.IntegerField(choices=verified_choices, default=False)
    spocomment = models.TextField(blank=True)

    def __str__(self):
        return self.string

class TechnicalSkill(models.Model):
    resume = models.ForeignKey('Resume', on_delete=models.CASCADE)
    title = models.TextField()
    description = models.TextField()

    def __str__(self):
        return self.title

class ProjectInternships(models.Model):
    resume = models.ForeignKey('Resume', on_delete=models.CASCADE)
    title = models.TextField()
    description = models.TextField()
    proof = models.FileField(upload_to='projectinternships', null=True)
    verified_choices = (
        (1, 'Not yet verified'),
        (2, 'Verified'),
        (3, 'Wrong Proof'),
    )
    verified = models.IntegerField(choices=verified_choices, default=False)
    spocomment = models.TextField(blank=True)

    def __str__(self):
        return self.title


class Resume(models.Model):

    def __str__(self):
        return "%s's resume %r" % (self.user, self.pk)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)

    CPI = models.FloatField(
        validators=[MaxValueValidator(10.0), MinValueValidator(0.0)]
    )

    years = ((1, '1'),
             (2, '2'),
             (3, '3'),
             (4, '4'),
             (5, '5'),
            )
    year = models.IntegerField(choices=years)

    departments = (
        ('CSE', 'Computer Science and Engineering'),
        ('MSE', 'Material Science and Engineering'),
        ('CHE', "Chemical Engineering"),
        ('BSBE', 'Biological Sciences and Bio Engineering'),
        ('AE', 'Aerospace Engineering'),
    )
    dept = models.CharField(max_length=4, choices=departments)

    email = models.EmailField()
    # alt_email = models.EmailField()

    phone = models.CharField(max_length=10)
    # alt_phone = models.CharField(max_length=10)

    # academics = models.OneToOneField(ACADEMICDETAILS, on_delete=models.CASCADE)

