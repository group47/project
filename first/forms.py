from django import forms
from django.contrib.auth.models import User
from . import models

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'first_name', 'last_name']
        widgets = {
            'password' : forms.PasswordInput()
        }
class ResumeForm(forms.ModelForm):
    class Meta:
        model = models.Resume
        fields = ['name', 'year', 'dept', 'email', 'phone', 'CPI']
        name = 'required'

    phone = forms.CharField(max_length=10, min_length=10)

class ExamForm(forms.ModelForm):
    class Meta:
        model = models.Exam
        fields = ['name', 'institute', 'cgpa_percentage', 'year', 'proof']


class ProjectForm(forms.ModelForm):
    class Meta:
        model = models.ProjectInternships
        fields = ['title', 'description', 'proof']
        widgets = {
            'title': forms.Textarea(attrs={'class':'materialize-textarea', 'id':'textarea'}),
            'description': forms.Textarea(attrs={'class':'materialize-textarea', 'id':'textarea'}),
        }

class TechnicalSkillForm(forms.ModelForm):
    class Meta:
        model = models.TechnicalSkill
        fields = ['title', 'description']
        widgets = {
            'title': forms.Textarea(attrs={'class':'materialize-textarea', 'id':'textarea'}),
            'description': forms.Textarea(attrs={'class':'materialize-textarea', 'id':'textarea'}),
        }

class ScholasticAchievementsForm(forms.ModelForm):
    class Meta:
        model = models.ScholasticAchievements
        fields = ['string', 'proof']
        widgets = {
            'string': forms.Textarea(attrs={'class':'materialize-textarea', 'id':'textarea'}),
        }

