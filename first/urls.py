from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^register/$', views.register_view, name='register_url'),
    url(r'^login/$', views.login_view, name='login_url'),
    url(r'^logout/$', views.logout_view, name='logout_url'),
    url(r'^make_resume/$', views.make_resume_view, name='make_resume_url'),
    url(r'^$', views.home_view, name='home_url'),
    url(r'^resume/(?P<resume_id>[0-9]+)/$', views.resume_detail_view, name='resume_detail_url'),
    url(r'^getpdf/(?P<resume_id>[0-9]+)/(?P<template_id>[0-9]+)/$', views.getpdf_view, name="getpdf_url"),
    url(r'^delete/(?P<resume_id>[0-9]+)/$', views.delete_resume_view, name='delete_url'),
    url(r'^information/$', views.information_view, name='information_url'),
    url(r'^view_template/(?P<template_id>[0-9]+)/$', views.view_template_view, name="view_template_url"),
]
